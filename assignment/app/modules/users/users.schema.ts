import { UUIDVersion } from "express-validator/src/options";
import {v4} from "uuid";
import {IMovies} from "./users.types"
//this is implementation file
class UserSchema {
    movies:IMovies[]=[];

//this user is being passed as req.body in user schema
    save(movie:IMovies){

        const id:string=v4();
        const movieCollection = {...movie,id};
        this.movies.push(movieCollection);

        return movieCollection
        
    }
    //no need to pass any parameter if we want all the data to be extracted


    getData(){
        return this.movies
    }

    updateMovie(movie:IMovies){
        const index=this.movies.findIndex(p=>p.id==movie.id);
        if(index>-1){
            this.movies[index]=movie;
            return this.movies
        }else{
            console.log("MOVIE NOT FOUND...")
        }
    }
    deleteMovie(id:string){
        
        const index=this.movies.findIndex(p=>p.id==id);
        if(index>-1){
            this.movies.splice(index,1)
            return this.movies;
        }else{
            console.log("MOVIE NOT DELETED....")
        }
    }
}
const newDatabase=new UserSchema();
export default newDatabase;