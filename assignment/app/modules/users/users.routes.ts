
 import {Router} from "express";
 import newDatabase from  "./users.service";
 const router=Router()
 router.post("/",(req,res,next)=>{
    try{
    //all req.body should be implemented here
        const movie= req.body;
   const newData=newDatabase.createMovie(movie);
    //need to add response
   res.send({message:'MOVIE ADDED..',data:newData})

   }catch(e){
       res.status(500).send({message:"ERROR OCCURED..."})
   }
});

router.get('/',(req,res,next)=>{
    try{
        const data=newDatabase.getMovie();
        res.send(data)
    }catch(e){
        res.status(500).send({message:"ERROR OCCURED..."})
    }
})


router.put('/',(req,res,next)=>{
    try{
        const movie=req.body;
        const data=newDatabase.updateMovie(movie);
    res.send({message:"MOVIE UPDATED...",data})
    }catch(e){
        res.status(500).send({message:"ERROR OCCURED...."})
    }
})

router.delete('/:id',(req,res,next)=>{
    try{
        const {id}=req.params;
        const data=newDatabase.deleteMovie(id);
        res.send({message:"DELETED...",data})
    }catch(e){
        res.send("MOVIE DELETED....")
    }
})
export default router