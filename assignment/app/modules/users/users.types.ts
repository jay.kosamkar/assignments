

export interface IMovies{
    name:string;
    price:number;
    duration :number;
    id:string;
}