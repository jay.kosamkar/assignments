import inFo from "./users.repo";
import {IMovies} from "./users.types"

const createMovie = (movie: IMovies) => inFo.createMovie(movie);
const getMovie=()=> inFo.getMovie();
 const updateMovie=(movie:IMovies)=>inFo.updateMovie(movie)
const deleteMovie= (id:string)=> inFo.deleteMovie(id)


export default {createMovie,getMovie,updateMovie,deleteMovie}